import sys
from hashlib import pbkdf2_hmac
import binascii

salt_ticket=sys.argv[1]
hostname=sys.argv[2]

salt_utf = salt_ticket.encode('utf-8')
hostname_utf = hostname.encode('utf-8')
token = binascii.hexlify(pbkdf2_hmac('sha1', hostname_utf, salt_utf,50000)).decode('utf-8')
print(token)

